$Id
### imce_watermark Readme

imce_watermark.module is the module for adding watermark to image in imce image uploading.

Installation:
	Installation is like all normal modules (e.g. extract in the directory sites/all/modules)
	The validlink module has one dependency: imce.

Configuration:
	The configuration page is at /admin/settings/imce/imce_watermark.

Caution:
  Watermark image can be only 8bit png file.

TODO:
	1. Add support for other Xbit png files;
  2. Research other image watermarking ways - faster, compatible with common image libraries, etc.
